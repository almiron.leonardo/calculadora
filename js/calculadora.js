let operacion;
let primerNumeral;
let segundoNumeral;
// PROBAR ALGUNA OTRA FORMA DE RESOLVER ASI QEUDA ORIGINAL. DEJARLO RESPONSIVE 

let uno = document.getElementById("uno")
let dos = document.getElementById("dos");
let tres = document.getElementById("tres");
let cuatro = document.getElementById("cuatro");
let cinco = document.getElementById("cinco");
let seis = document.getElementById("seis");
let siete = document.getElementById("siete");
let ocho = document.getElementById("ocho");
let nueve = document.getElementById("nueve");
let cero = document.getElementById("cero");
let igual = document.getElementById("igual");
let sumar = document.getElementById("sumar");
let restar = document.getElementById("restar");
let dividir = document.getElementById("dividir");
let multiplicar = document.getElementById("multiplicar");
let decimal = document.getElementById("decimal");
let resultado = document.getElementById("resultado");

uno.onclick = function () {
    resultado.textContent = resultado.textContent + "1";
}
dos.onclick = function () {
    resultado.textContent = resultado.textContent + "2";
}
tres.onclick = function () {
    resultado.textContent = resultado.textContent + "3";
}
cuatro.onclick = function () {
    resultado.textContent = resultado.textContent + "4";
}
cinco.onclick = function () {
    resultado.textContent = resultado.textContent + "5";
}
seis.onclick = function () {
    resultado.textContent = resultado.textContent + "6";
}
siete.onclick = function () {
    resultado.textContent = resultado.textContent + "7";
}
ocho.onclick = function () {
    resultado.textContent = resultado.textContent + "8";
}
nueve.onclick = function () {
    resultado.textContent = resultado.textContent + "9";
}
cero.onclick = function () {
    resultado.textContent = resultado.textContent + "0";
}
sumar.onclick = function () {
    primerNumeral = resultado.textContent;
    operacion = "+";
    limpiar();
}
restar.onclick = function () {
    primerNumeral = resultado.textContent;
    operacion = "-";
    limpiar();
}
dividir.onclick = function () {
    primerNumeral = resultado.textContent;
    operacion = "/";
    limpiar();
}
multiplicar.onclick = function () {
    primerNumeral = resultado.textContent;
    operacion = "*";
    limpiar();
}
igual.onclick = function () {
    segundoNumeral = resultado.textContent;
    solucion();
}
borra.onclick = function () {
    borrar();
}
decimal.onclick = function () {
    resultado.textContent = resultado.textContent + ".";
}

function limpiar() {
    resultado.textContent = "";
}

function borrar() {
    resultado.textContent = "";
    primerNumeral = 0;
    segundoNumeral = 0;
    operacion = "";
}

function solucion() {
    let sol = 0;
    switch (operacion) {
        case "+":
            sol = parseFloat(primerNumeral) + parseFloat(segundoNumeral);
            break;
        case "-":
            sol = parseFloat(primerNumeral) - parseFloat(segundoNumeral);
            break;
        case "/":
            sol = parseFloat(primerNumeral) / parseFloat(segundoNumeral);
            break;
        case "*":
            sol = parseFloat(primerNumeral) * parseFloat(segundoNumeral);
            break;
    }
    borrar();
    resultado.textContent = sol;
}

